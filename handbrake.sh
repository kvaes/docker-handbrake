#!/bin/bash

LOCK=/tmp/handbrake.lock
SDIR=/data/in
TDIR=/data/temp
ODIR=/data/out

if [ ! -f $LOCK ]
then
  echo "no lock, setting lock"
  touch $LOCK
else
  echo "locked, exiting"
  exit 1
fi

cd $SDIR
$(for f in *\ *; do mv "$f" "${f// /_}"; done)

for sfile in $SDIR/*.*
do
        tfile=$(basename $sfile)
        if [ ! -f $TDIR/$tfile.mp4 ] && [ ! -f $TDIR/$tfile.done ]
        then
                if [ -f "$sfile" ]
                then
                        echo "*** Processing $tfile ***"
                        HandBrakeCLI -i $sfile -o $TDIR/$tfile.mp4 --preset="iPad"
                        mv $sfile $TDIR/$tfile.done
                fi
        else
                echo "*** File $tfile already exists... Ignoring ***"
        fi
	mv $TDIR/* $ODIR/
done

echo "unlocking"
rm $LOCK
